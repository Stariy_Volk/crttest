/**
 * Created by vatiba01 on 29.03.2016.
 */

var DropArea = function(selector){
    var data = {};

    data.reader = new FileReader();

    function createEvents(){
        data.elem.on("dragover", function(e){
            e.preventDefault();
            $(this).addClass("hover");
        });
        data.elem.on("dragleave", function(e){
            e.preventDefault();
            $(this).removeClass("hover");
        });
        data.elem.on("drop", function(e){
            e.preventDefault();
            $(this).removeClass("hover");
            $(this).addClass("drop");
            processFiles(e.originalEvent.dataTransfer.files);
            console.log(e.originalEvent.dataTransfer.files);
        });
        data.reader.onload = function(event){
            //data.audio.src = event.target.result;
            //data.audio.play();
            var res = event.target.result;
            if(res.indexOf("data:audio/mp3;base64,") + 1 || res.indexOf("data:audio/wav;base64,") + 1){
                audioApi.loadSong(event.target.result.replace("data:audio/mp3;base64,","").replace("data:audio/wav;base64,",""));
            } else {
                $("#filename").text("Неверный формат файла");
            }
        };
    }

    function processFiles(files){
        var test = '',
            reader = '',
            count = 0,
            file = '';
        for (var num in files) {
            file = files[num];
            if(typeof file == 'object' && count == 0){
                data.reader.readAsDataURL(file);
                $("#filename").text(file.name);
                $("#history").prepend("<li><span>" + file.name + "</span><span></span></li>");
                count++;
            }
        }
    }

    return {
        init: function(){
            data.elem = $(selector);
            if(data.elem.length){
                createEvents();
                //data.audio = $("<audio></audio>")[0];
                //data.audio2 = $("<audio></audio>")[0];
            }
        }
    }
};

var drop = '';
(function(){
    drop = new DropArea("#drop");
    drop.init();
})();