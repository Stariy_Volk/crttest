/**
 * Created by vatiba01 on 29.03.2016.
 */

var AudioApi = function(){
    var data = {},
        fbc_array, bars, bar_x, bar_width, bar_height, save = true, reloop = false;

    data.time = 0;
    data.startTime = parseInt((new Date()) /1000);
    data.endTime = parseInt((new Date()) /1000);

    function addEvents(){
        $("#play").on("click", function(e){
            e.preventDefault();
            if(data.source){
            } else {
                getData(data.byteArray, function(){
                    data.source.start(0, data.time);
                    data.state = true;
                    data.startTime = parseInt((new Date()) /1000) - data.time;

                    reloop = true;

                });
            }
        });
        $("#stop").on("click", function(e){
            e.preventDefault();
            toStop();
            data.state = false;
        });
    }

    function toStop(){
        if(data.ctx.state === 'running'){
            data.source.stop();
            delete data.source;
            reloop = false;
        }
    }

    function draw() {

            data.canvas.save();
            data.canvas.fillStyle = 'rgb(200, 200, 200)';
            data.canvas.fillRect(0, 0, data.canvasElem.width(), data.canvasElem.height());
            data.canvas.restore();

            drawVisual = requestAnimationFrame(draw) || webkitRequestAnimationFrame(draw);
        if(reloop){
            drawByteTimeDomainData();
            data.time = parseInt((new Date()) /1000) - data.startTime;
        }
    }

    function drawByteFrequencyData () {
        var bufferLength = data.analyser.frequencyBinCount;
        var dataArray = new Uint8Array(bufferLength);
        data.analyser.getByteFrequencyData(dataArray);
        for ( var i = 0; i < (bufferLength/3); i++ ) {
            var value = dataArray[i*3];
            var percent = value / 256;
            var height = data.canvasElem.height() * percent;
            var offset = data.canvasElem.height()/2 - height;
            var barWidth = data.canvasElem.width() / data.analyser.frequencyBinCount;
            var hue = i / data.analyser.frequencyBinCount * 760;
            data.canvas.save();
            data.canvas.fillStyle = 'hsla(' + hue + ', 100%, 50%, 0.5)';
            data.canvas.fillRect(i * barWidth, offset, barWidth, height);
            data.canvas.restore();
        }
    }

    function drawByteTimeDomainData () {
        var bufferLength = data.analyser.frequencyBinCount;
        var dataArray = new Uint8Array(bufferLength);
        data.analyser.getByteTimeDomainData(dataArray);
        data.canvas.lineWidth = 1;
        data.canvas.strokeStyle = 'rgb(0, 0, 0)';
        data.canvas.lineJoin = data.canvas.lineCap = 'round';
        data.canvas.beginPath();
        var sliceWidth = data.canvasElem.width() * 1.0 / bufferLength;
        var x = 0;
        for(var i = 0; i < bufferLength; i++) {
            var value = dataArray[i] / 128.0;
            var y = value * data.canvasElem.height() / 2 - data.canvasElem.height() / 4;
            if(i === 0) {
                data.canvas.moveTo(x, y);
            } else {
                data.canvas.lineTo(x, y);
            }
            x += sliceWidth;
        }
        data.canvas.lineTo(data.canvasElem.width(), data.canvasElem.height() / 2);
        data.canvas.stroke();
    }

    function getData(byteArray, callback){
        data.ctx.decodeAudioData(byteArray, function(buffer){
            if(!data.source){
                data.source = data.ctx.createBufferSource();
                data.source.buffer = buffer;
                data.gainNode = data.ctx.createGain();
                data.analyser = data.ctx.createAnalyser();
                data.gainNode.gain.value = 0.1;

                data.source.connect(data.analyser);
                data.analyser.connect(data.gainNode);
                data.gainNode.connect(data.ctx.destination);

                $("#play").addClass("ready");

                data.source.onended = onEnded;
                if(callback){
                    callback();
                }
            }
        },function(err){
            console.log(err);
        });
    }

    function onEnded(){

        delete data.source;
        if(data.state){
            console.log("ended");
            data.time = 0;
        } else {
            //$("#history").prepend("<li>" + $("#filename").text() + "</li>");
            console.log("paused");

        }
    }

    return {
        init: function(){
            data.ctx = new (window.AudioContext || window.webkitAudioContext)();
            if(data.ctx){
                addEvents();
                data.canvasElem = $("#osc");
                data.canvas = data.canvasElem[0].getContext('2d');
                data.history = $("#history");
                draw();
                data.history.html('');
            }
        },
        loadSong: function(content){
            if(data.source){
                toStop();
            }
            data.byteArray = Base64Binary.decodeArrayBuffer(content);
            data.time = 0;
        }
    }
};

var audioApi = '';

(function(){
    audioApi = new AudioApi();
    audioApi.init();
})();
