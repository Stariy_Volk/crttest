var gulp = require('gulp'),
	less = require('gulp-less'),
    minifycss = require('gulp-minify-css'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	clean = require('gulp-clean'),
	watch = require('gulp-watch'),
	jade = require('gulp-jade'),
	rename = require('gulp-rename'),
	imagemin = require('gulp-imagemin'),
	newer = require('gulp-newer'),
	pngquant = require('imagemin-pngquant'),
	LessPluginAutoPrefix = require('less-plugin-autoprefix'),

	autoprefix = new LessPluginAutoPrefix({browsers: ["> 5%"]}),

	src = {
		css: '_src/css/**/*',
		less: '_src/css/style.less',
		js: '_src/js/modules/*',
		img: '_src/img/**/*',
        jadePages: '_src/jade/pages/*.jade',
        jadeBlocks: '_src/jade/blocks/*.jade',
        jadeDataJson: './_src/jade/data.json'
    },
    dataJson = require(src.jadeDataJson);

//Получаем css
gulp.task('less', function () {
    return gulp.src(src.less)
        .pipe(less({
            plugins: [autoprefix]
        }))
        .pipe(gulp.dest('build/css'));
});

//Минифицируем css
gulp.task('minCss', function () {
    return gulp.src('build/css/style.css')
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('build/css'));
});

//Получаем js
gulp.task('js', function() {
    return gulp.src(src.js)
        .pipe(newer('js/main.js'))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('build/js'))
});

//Минифицируем и обфусцируем js
gulp.task('minJs', function() {
    return gulp.src('build/js/main.js')
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('build/js'));
});

//Жмем картинки
gulp.task('imagemin', function () {
    return gulp.src(src.img)
        .pipe(newer('build/img'))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('build/img'));
});

//Получаем страницы и блоки html из jade
gulp.task('jade', function() {
    gulp.src(src.jadePages)
        .pipe(jade({
            locals: dataJson,
            pretty: '\t'
        }))
        .pipe(gulp.dest('build/tpl'));

    gulp.src(src.jadeBlocks)
        .pipe(jade({
            locals: dataJson,
            pretty: '\t'
        }))
        .pipe(gulp.dest('build/tpl/blocks'));
});

// Чистим css & img
gulp.task('clean', function() {
    return gulp.src(['build/css', 'build/img'], {read: false})
        .pipe(clean());
});

// Действия по умолчанию
// Можно раскомментировать clean, чтобы в папке build убрать мусор
gulp.task('default', /*['clean'],*/ function(){
    gulp.start('jade', 'js', 'less', 'imagemin');

    // Отслеживаем изменения в файлах
    gulp.watch(src.js, ['js']);
    gulp.watch(src.css, ['less']);
    gulp.watch([src.jadeBlocks, src.jadePages, src.jadeDataJson], ['jade']);
    gulp.watch(src.img, ['imagemin']);
});